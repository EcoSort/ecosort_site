<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/about.css" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .about {
                height: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="antialiased">

    <header>
            <div>
                <img src="img/logo.png" height="50px">
                <label>ECOSORT</label>
            </div>
            <div>
                <ul>
                    <li><a href="/home" class="home">HOME</a></li>
                    <li><a href="/about"class="about">ABOUT</a></li>
                    <li><a href="/projects" class="projects">PROJECTS</a></li>
                    <li><a href="/tries" class="tries">TRIES</a></li>
                    <li><a href="/analyser" class="tries">ANALYSER</a></li>

                </ul>
            </div>
        </header>
    <main>
        <div>
            <img src="img/eco_monde.png" height="5%">
        </div>
        <div>
            <artcile>
            <h2>Ecologie</h2> 
            <p>L'écologie est une science et un mouvement social qui étudie les interactions entre les êtres vivants et leur environnement. Face aux défis environnementaux tels que le changement climatique et la perte de biodiversité, il est crucial d'agir. Cela inclut la transition vers des énergies renouvelables, la promotion de la consommation durable, la gestion des déchets, la protection des habitats naturels, et l'adoption de politiques environnementales efficaces. Chacun a un rôle à jouer, de la consommation responsable à l'éducation et à la sensibilisation. Ensemble, nous pouvons œuvrer pour un avenir durable et équilibré pour la planète.</p>
            </artcile>
            <a href="https://fr.wikipedia.org/wiki/%C3%89cologie">READ MORE</a>
        </div>        

    </main>

    </body>
</html>