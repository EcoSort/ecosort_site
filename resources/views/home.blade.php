<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/home.css" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .home {
                height: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="antialiased">
        <header>
            <div>
                <img src="img/logo.png" height="50px">
                <label>ECOSORT</label>
            </div>
            <div>
                <ul>
                    <li><a href="/home" class="home">HOME</a></li>
                    <li><a href="/about"class="about">ABOUT</a></li>
                    <li><a href="/projects" class="projects">PROJECTS</a></li>
                    <li><a href="/tries" class="tries">TRIES</a></li>
                    <li><a href="/analyser" class="tries">ANALYSER</a></li>

                </ul>
            </div>
        </header>
        <main>
            <h1>EcoSort, apprenez en tier en quelques secondes!</h1>
            <div class="search">
            <input type="text" value="Plastique" id="myInput">
            <button id="search" onclick="Search()">🔍</button>
            <p id="rt_tri"></p>
            </div>
            <section>
            <p>Ici, nous explorons les enjeux environnementaux qui façonnent notre planète et les solutions qui contribuent à sa préservation. Que vous soyez passionné par la biodiversité, le changement climatique, la conservation des écosystèmes ou encore les modes de vie durables, vous trouverez ici une source d'inspiration et d'information.</p>
            <img src="img/home_poubelle.png">
            <img src="img/home_eco.png">
            <img src="img/home_tri.png">
        </section>
        </main>
    </body>
    
    
</html>
<script>
    

    var tab = {'Plastique' : "Bac de tri", "Métal" : "Bac de tri", "Carton" : "Bac de tri", "Papier" : "Bac de tri", "Verre" : "Borne à verre", "Aliment" : "Composteur", "caoutchouc" : "Bac noir",  "Aliment non mengeable" : "Bac noir"};

    function Search(){
        myinput = document.getElementById('myInput').value ;
        for(var key in tab){
            if (myinput == key){
                document.getElementById("rt_tri").innerHTML = tab[key] ;
            }
        }}
</script>
