<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/projects.css" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .projects{
                height: 10px;
                font-weight: bold;
            }

        </style>
    </head>
    <body class="antialiased">
    <header>
            <div>
                <img src="img/logo.png" height="50px">
                <label>ECOSORT</label>
            </div>
            <div>
                <ul>
                    <li><a href="/home" class="home">HOME</a></li>
                    <li><a href="/about"class="about">ABOUT</a></li>
                    <li><a href="/projects" class="projects">PROJECTS</a></li>
                    <li><a href="/tries" class="tries">TRIES</a></li>
                    <li><a href="/analyser" class="tries">ANALYSER</a></li>

                </ul>
            </div>
        </header>

        <main>
            <section>
        <div>
            <img src="img/eco_projet.png" height="5%">
        </div>
        <div>
            <artcile>
            <h2>Recyclage IA</h2> 
            <p class="bold"> le recyclage en matière d'IA vise à optimiser et à réutiliser les ressources, que ce soit des modèles, des données ou des ressources informatiques, afin de réduire l'empreinte environnementale de ces systèmes et de maximiser leur efficacité. Cela contribue à un développement durable de l'IA.</p>
        </artcile>
            <a href="https://fr.wikipedia.org/wiki/Recyclage">READ MORE</a>
        </div> 
        </section>   
        <section>
        <div>
            <artcile>
            <h2>Concept</h2> 
            <p>Les poubelles de recyclage sont des conteneurs spécialement conçus pour trier et collecter des matériaux recyclables comme le papier, le verre, le plastique et le métal. Chaque poubelle est dédiée à un type spécifique de matériau. Ces matériaux sont ensuite traités dans des centres de recyclage pour être transformés en nouvelles matières premières réutilisables. Cette pratique contribue à réduire la consommation de ressources naturelles et à minimiser l'impact environnemental des déchets.</p>
        </artcile>
            <a href="https://fr.wikipedia.org/wiki/Tri_des_d%C3%A9chets">READ MORE</a>
        </div> 
        <div>
            <img src="img/eco_projet_poubelle.png" height="5%">
        </div>
        
        </section>     

    </main> 

    </body>
</html>