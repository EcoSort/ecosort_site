<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/tries.css" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .tries {
                height: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="antialiased">
 <header>
            <div>
                <img src="img/logo.png" height="50px">
                <label>ECOSORT</label>
            </div>
            <div>
                <ul>
                    <li><a href="/home" class="home">HOME</a></li>
                    <li><a href="/about"class="about">ABOUT</a></li>
                    <li><a href="/projects" class="projects">PROJECTS</a></li>
                    <li><a href="/tries" class="tries">TRIES</a></li>
                    <li><a href="/analyser" class="analyser">ANALYSER</a></li>
                </ul>
            </div>
    </header>
    <main>
        <section>
        <h1>LES DIFFERENTS TRIES</h1>
        <p>Les poubelles de tri sélectif permettent la collecte spécifique de matériaux recyclables comme le papier, le verre, le plastique et le métal. Chacune est dédiée à un type de matériau. Ces déchets sont ensuite traités pour être transformés en nouvelles matières premières, favorisant ainsi le recyclage et la préservation de l'environnement.</p>
        </section>
        <h2>Notons ci-dessous un exemple:</h2>
        <div>
            <article>
                <img src="img/poubelle_jaune.png">
                <p>La poubelle jaune est un bac à ordure très polyvalent, car elle peut contenir de nombreux types de déchets. </p>
            </article>
            <article>
                <img src="img/poubelle_bleu.png">
                <p>La poubelle bleue est dédiée à la collecte du papier et du carton, tels que journaux, magazines et cartons d'emballage</p> 
            </article>
            <article>
                <img src="img/poubelle_verte.png">
                <p>vous pouvez placer les déchets organiques comme les restes de cuisine (fruits, légumes, coquilles d'œufs) et les déchets de jardin</p>
            </article>
            <article>
                <img src="img/poubelle_marron.png">
                <p>Le bac marron est destiné aux déchets verts et au carton.</p>
        </article>
            </div>
    
    </main>


    </body>
</html>