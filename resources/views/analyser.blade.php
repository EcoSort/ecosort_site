<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/header.css" />
        <link rel="stylesheet" type="text/css" href="css/analyser.css" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .analyser {
                height: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="antialiased">
        <header>
            <div>
                <img src="img/logo.png" height="50px">
                <label>ECOSORT</label>
            </div>
            <div>
                <ul>
                    <li><a href="/home" class="home">HOME</a></li>
                    <li><a href="/about"class="about">ABOUT</a></li>
                    <li><a href="/projects" class="projects">PROJECTS</a></li>
                    <li><a href="/tries" class="tries">TRIES</a></li>
                    <li><a href="/analyser" class="analyser">ANALYSER</a></li>

                </ul>
            </div>
        </header>
        <main>
            <div>
                
                <article>
                    <h1>EcoSort - Scan pour trier</h1>
                    <a href='#'>EcoSort, inc.</a>
                    <div>
                    <section>
                        <span>4,3✦</span>
                        <p>14 avis</p>
                    </section>
                    <section class='milieu_desc'>
                        <span>8+</span>
                        <p>Téléchargements</p>
                    </section>
                    <section>
                        <img src="img/home_classement.png">
                        <p>PEGI 3</p>
                    </section>
                    </div>
                <section class ='access'>
                 <a href="#">Installer</a>
                </section>
                </article>
                <article>
                    <img src="img/analyser_logo.png" class="logoDownload">
                </article>  
             </div>
            
            </div>
        </main>
    </body>
</html>
